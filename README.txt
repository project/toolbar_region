The Toolbar region module adds a region to the Toolbar module, allowing the
placement of any blocks into the Toolbar, much like the functionality of the
Admin module.

Toolbar region was written by Stuart Clark (deciphered) and is maintained by
Stuart Clark and Brian Gilbert (realityloop) of Realityloop Pty Ltd.
- http://realityloop.com
- http://twitter.com/Realityloop



Features
--------------------------------------------------------------------------------

- Add any block to the Toolbar via the Block layout interface.
- Remove or re-arrange existing Toolbar items.
- Adjust name and icons of Toolbar items.



Configuration
--------------------------------------------------------------------------------

Tab title and icons can be set on the Block settings form for the specific
block.



Roadmap
--------------------------------------------------------------------------------

* Add support for custom icons.
